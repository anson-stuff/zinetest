# Anson's Aggregated Feed

Anson's Aggregated Feed is a Rust application designed to aggregate content from multiple RSS feeds, creating a personalized news page. It fetches articles from specified RSS feed URLs, generates HTML cards for each entry, and outputs a single, styled HTML page. This project showcases the use of Rust for web content aggregation and manipulation, leveraging several powerful crates for parsing RSS feeds, handling dates and times, making HTTP requests, and rendering HTML.
