use feed_rs::parser;
use reqwest::blocking::get;
use std::fs;

// Function to read URLs from a file
fn read_feed() -> Vec<String> {
    let binding = fs::read_to_string("feeds.txt").unwrap();
    binding.lines().map(|s| s.to_owned()).collect()
}

// Function to fetch and parse a feed, returning true if successful
fn fetch_and_parse_feed(url: &str) -> bool {
    let content = match get(url) {
        Ok(response) => response.text().unwrap_or_default(),
        Err(_) => return false,
    };

    parser::parse(content.as_bytes()).is_ok()
}

#[test]
fn test_that_urls_point_to_valid_feeds() {
    let urls = read_feed();

    for url in urls {
        assert!(
            fetch_and_parse_feed(&url),
            "Feed at URL failed validation: {}",
            url
        );
    }
}

#[test]
fn test_if_feeds_are_in_alphabetical_order() {
    let mut urls = read_feed();

    if !urls.windows(2).all(|w| w[0] < w[1]) {
        println!("Sorted feeds.txt:");

        urls.sort();

        for url in urls {
            println!("{}", url);
        }
        panic!("\nfeeds.txt was not sorted!")
    }
}
